# TypeRacer Bot

With puppeteer, it can read the text and simulate typing automatically.

**Note:** Puppeteer default engine (Chromium) doesn't pass the capcha test before entering the game

Work around is to run Chrome in debug mode and use Puppeteer via websocket

To run Chrome in debug mode

```
/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --remote-debugging-port=9222 --no-first-run --no-default-browser-check --user-data-dir=$(mktemp -d -t 'chrome-remote_data_dir')
```

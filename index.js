const puppeteer = require("puppeteer");

const BROWSER =
  "ws://127.0.0.1:9222/devtools/browser/1b147135-1805-4d91-9b59-e68365f3a6f6";

const LINK = "https://play.typeracer.com";
// const LINK = "https://play.typeracer.com?rt=1ihribm1bq";

const ENTER_RACE_SELECTOR = ".enterRace a";
const WORDS_SELECTOR = ".mainViewportHolder .inputPanel table td span";
const COUNTER_SELECTOR = ".countdownPopup.horizontalCountdownPopup";
const INPUT_SELECTOR = ".mainViewportHolder .inputPanel input.txtInput";
const TYPE_INDICATOR_SELECTOR = ".arrowPopup.arrowPopup-start";

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

const SEED = [
  [500, 1600],
  [700, 900],
  [900, 1200],
  [600, 700],
  [450, 800],
  [450, 600],
  [500, 800],
  [700, 800],
  [550, 600],
  [600, 700],
  [600, 700],
  [500, 600],
  [500, 700],
  [800, 900],
  [900, 1000],
  [650, 1000],
  [800, 1600],
  [850, 900],
];

const getSeed = () => {
  return SEED[Math.floor(Math.random() * SEED.length)];
};

const randBetween = (from, to) => {
  return Math.floor(Math.random() * to + 1) - from;
};

const joinRace = async (page) => {
  console.log("JOIN RACE ===============");
  await page.waitForSelector(ENTER_RACE_SELECTOR);
  await sleep(500);
  await page.click(ENTER_RACE_SELECTOR);
  await sleep(2000);
};

const readWords = async (page) => {
  console.log("READ WORDS ===============");
  await page.waitForSelector(WORDS_SELECTOR);
  await sleep(2000);
  const options = await page.$$(WORDS_SELECTOR);
  const words = [];
  for (const option of options) {
    const word = await page.evaluate((el) => el.innerText, option);
    words.push(word);
  }
  return words.join("");
};

const typeWords = async (page, words) => {
  console.log("WAIT FOR COUNTER ===============");
  // await page.waitForSelector(COUNTER_SELECTOR, { hidden: true });
  await page.waitForSelector(TYPE_INDICATOR_SELECTOR);
  await sleep(300);
  console.log("TYPING ===============");
  const input = await page.$(INPUT_SELECTOR);
  await input.type(words, { delay: 112 }); // MAGICAL NUMBER 112
  // for (const c of words.split("")) {
  //   const seed = getSeed();
  //   const delay = randBetween(seed[0], seed[1]);
  //   await sleep(delay);
  //   await input.type(c);
  // }
};

const testNewFunction = async (hello) => {
  await sleep(200);
  console.log("ggwp", hello);
};

(async () => {
  // const browser = await puppeteer.launch({ headless: false });
  const browser = await puppeteer.connect({ browserWSEndpoint: BROWSER });
  const page = await browser.newPage();
  await page.goto(LINK);

  await joinRace(page);
  const words = await readWords(page);
  console.log("WORDS", words);
  await typeWords(page, words);

  console.log("END!");
  await page.screenshot({ path: "result.png" });
})();
